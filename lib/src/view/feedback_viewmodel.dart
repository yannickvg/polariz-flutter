import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:polariz/feedback.dart';
import 'package:polariz/src/domain/feedback_handler.dart';
import 'package:polariz/src/domain/impl/jira/jira_config.dart';
import 'package:polariz/src/domain/impl/jira/jira_feedback_handler.dart';

enum FeedbackState { pending, submitting, success, error }

class FeedbackViewModel extends ChangeNotifier {
  bool _isVisible = false;
  List<FeedbackHandler> _handlers = [];

  final GetAdditionalAttachments? additionalAttachments;
  final GetUserId? getUserId;
  final OnFeedbackGiven? onFeedbackGiven;
  final JiraConfig? jiraConfig;
  String _message = "";
  OnFeedbackGiven? _temporaryOnFeedbackGiven;

  FeedbackState state = FeedbackState.pending;

  FeedbackViewModel(
      {this.getUserId, this.additionalAttachments, this.jiraConfig, this.onFeedbackGiven}) {
    if (jiraConfig != null) {
      _handlers.add(JiraFeedbackHandler(jiraConfig!));
    }
  }

  bool get isVisible => _isVisible;

  String get message => _message;

  void show({onFeedbackGiven}) {
    _isVisible = true;
    _temporaryOnFeedbackGiven = onFeedbackGiven;
    notifyListeners();
  }

  void hide() {
    _isVisible = false;
    _message = "";
    notifyListeners();
  }

  void handleFeedback(Uint8List screenshot) async {
    state = FeedbackState.submitting;
    notifyListeners();
    var _result = true;
    String? userId;
    if (getUserId != null) {
      userId = await getUserId!();
    }
    List<File> attachments = [];
    if (additionalAttachments != null) {
      attachments = await additionalAttachments!();
    }
    await Future.forEach(_handlers, (FeedbackHandler handler) async {
      _result &= await handler.handleFeedback(userId, message, screenshot, attachments);
    });

    if (_result) {
      state = FeedbackState.success;
    } else {
      state = FeedbackState.error;
    }
    notifyListeners();
    await Future.delayed(Duration(seconds: 3), () {
      if (onFeedbackGiven != null) {
        onFeedbackGiven!(_result);
      }
      if (_temporaryOnFeedbackGiven != null) {
        _temporaryOnFeedbackGiven!(_result);
      }
      hide();
      _temporaryOnFeedbackGiven = null;
      Future.delayed(Duration(milliseconds: 500), () {
        state = FeedbackState.pending;
        notifyListeners();
      });
    });
  }

  void updateMessage(String message) {
    _message = message;
    notifyListeners();
  }
}
