import 'dart:io';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:polariz/src/domain/impl/jira/jira_config.dart';
import 'package:polariz/src/view/components/feedback_widget.dart';
import 'package:polariz/src/view/feedback_viewmodel.dart';
import 'package:polariz/src/view/translation/translation.dart';
import 'package:shake/shake.dart';

/// This widget should be at the top of your widget tree.
class PolarizFeedback extends StatefulWidget {
  const PolarizFeedback({
    Key? key,
    required this.child,
    this.onFeedbackGiven,
    this.getUserId,
    this.additionalAttachments,
    this.backgroundColor,
    this.drawColors,
    this.translation,
    this.jiraConfig,
    this.textDirection = TextDirection.ltr,
  }) : super(key: key);

  /// Gets called before submitting to get additional feedback.
  final GetAdditionalAttachments? additionalAttachments;

  /// The application to wrap, typically a [MaterialApp].
  final Widget child;

  /// The background of the feedback view.
  final Color? backgroundColor;

  /// Colors which can be used to draw on [child].
  final List<Color>? drawColors;

  /// Optional translation for the feedback view, defaults to english.
  final FeedbackTranslation? translation;

  /// Optional label to identify the user
  final GetUserId? getUserId;

  /// Optional config  for JIRA. If this config is present, it will post to JIRA
  final JiraConfig? jiraConfig;

  /// Optional textDirection
  final TextDirection textDirection;

  /// Optional callback that is called when the feedback is given.
  /// It has a boolean parameter to indicate if the feedback was successful or not
  final OnFeedbackGiven? onFeedbackGiven;

  /// Call `BetterFeedback.of(context)` to get an instance of
  /// [FeedbackData] on which you can call `.show()` or `.hide()`
  /// to enable or disable the feedback view.
  static FeedbackData? of(BuildContext context) =>
      context.dependOnInheritedWidgetOfExactType<FeedbackData>();

  @override
  _PolarizFeedbackState createState() => _PolarizFeedbackState();
}

class _PolarizFeedbackState extends State<PolarizFeedback> {
  late FeedbackViewModel viewModel;
  late ShakeDetector detector;

  @override
  void initState() {
    super.initState();
    viewModel = FeedbackViewModel(
        getUserId: widget.getUserId,
        additionalAttachments: widget.additionalAttachments,
        jiraConfig: widget.jiraConfig,
        onFeedbackGiven: widget.onFeedbackGiven);
    detector = ShakeDetector.autoStart(
        shakeThresholdGravity: 4,
        onPhoneShake: () {
          viewModel.isVisible ? viewModel.hide() : viewModel.show();
        });
    viewModel.addListener(onUpdateOfController);
  }

  @override
  void dispose() {
    super.dispose();
    detector.stopListening();
    viewModel.removeListener(onUpdateOfController);
  }

  @override
  Widget build(BuildContext context) {
    return MediaQuery(
      data: new MediaQueryData.fromWindow(ui.window),
      child: Directionality(
        textDirection: widget.textDirection,
        child: FeedbackData(
          viewModel: viewModel,
          child: GestureDetector(
            onTap: () {
              FocusScopeNode currentFocus = FocusScope.of(context);
              if (!currentFocus.hasPrimaryFocus) {
                currentFocus.unfocus();
              }
            },
            child: FeedbackWidget(
                child: widget.child,
                isFeedbackVisible: viewModel.isVisible,
                backgroundColor: widget.backgroundColor,
                drawColors: widget.drawColors,
                translation: widget.translation ?? EnTranslation(),
                viewModel: viewModel),
          ),
        ),
      ),
    );
  }

  void onUpdateOfController() {
    setState(() {});
  }
}

typedef OnFeedbackGiven = void Function(bool success);

class FeedbackData extends InheritedWidget {
  const FeedbackData({Key? key, required Widget child, required this.viewModel})
      : super(key: key, child: child);

  final FeedbackViewModel viewModel;

  @override
  bool updateShouldNotify(FeedbackData oldWidget) {
    return oldWidget.viewModel != viewModel;
  }

  void show({OnFeedbackGiven? onFeedbackGiven}) => viewModel.show(onFeedbackGiven: onFeedbackGiven);

  void hide() => viewModel.hide();

  bool get isLoading => viewModel.state == FeedbackState.submitting;
  bool get isSuccess => viewModel.state == FeedbackState.success;
  bool get isError => viewModel.state == FeedbackState.error;
  bool get isPending => viewModel.state == FeedbackState.pending;

  bool get isVisible => viewModel.isVisible;
}

/// Function which gets called before the user submits his feedback.
/// The additional files will also be processed when submitting the feedback
typedef GetAdditionalAttachments = Future<List<File>> Function();

/// Function which gets called before the user submits his feedback.
/// When it returns something, this is used to identify the user
typedef GetUserId = Future<String> Function();
