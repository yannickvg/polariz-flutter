import 'package:flutter/material.dart';
import 'package:polariz/src/view/painting/painter.dart';

class PaintOnBackground extends StatelessWidget {
  const PaintOnBackground({
    Key? key,
    required this.child,
    required this.isPaintingActive,
    required this.controller,
  }) : super(key: key);

  final Widget child;
  final bool isPaintingActive;
  final PainterController controller;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        child,
        if (isPaintingActive) Painter(controller),
      ],
    );
  }
}
