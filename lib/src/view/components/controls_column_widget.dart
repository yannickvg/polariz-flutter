import 'package:flutter/material.dart';
import 'package:polariz/src/view/components/icon_button_widget.dart';
import 'package:polariz/src/view/translation/translation.dart';

enum ControlMode {
  draw,
  navigate,
}

class ControlsColumn extends StatelessWidget {
  ControlsColumn({
    Key? key,
    required this.mode,
    required this.activeColor,
    required this.onColorChanged,
    required this.onUndo,
    required this.onControlModeChanged,
    required this.onClearDrawing,
    required this.colors,
    required this.translation,
  }) : super(key: key);

  final ValueChanged<Color> onColorChanged;
  final VoidCallback onUndo;
  final ValueChanged<ControlMode> onControlModeChanged;
  final VoidCallback onClearDrawing;
  final List<Color> colors;
  final Color activeColor;
  final FeedbackTranslation translation;
  final ControlMode mode;

  @override
  Widget build(BuildContext context) {
    final isNavigatingActive = ControlMode.navigate == mode;
    return Card(
      elevation: 8.0,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(12),
        ),
      ),
      clipBehavior: Clip.antiAlias,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          RotatedBox(
            quarterTurns: 1,
            child: MaterialButton(
              key: const Key('navigate_button'),
              child: Text(
                translation.navigate,
                style: TextStyle(color: isNavigatingActive ? Colors.black : Colors.grey),
              ),
              onPressed: () {
                onControlModeChanged(ControlMode.navigate);
              },
            ),
          ),
          _ColumnDivider(),
          RotatedBox(
            quarterTurns: 1,
            child: MaterialButton(
              key: const Key('draw_button'),
              minWidth: 20,
              child: Text(
                translation.draw,
                style: TextStyle(color: !isNavigatingActive ? Colors.black : Colors.grey),
              ),
              onPressed: () {
                onControlModeChanged(ControlMode.draw);
              },
            ),
          ),
          FeedbackIconButton(
            key: const Key('undo_button'),
            icon: Icon(Icons.undo, color: !isNavigatingActive ? Colors.black : Colors.grey),
            onPressed: isNavigatingActive ? null : onUndo,
          ),
          FeedbackIconButton(
            key: const Key('clear_button'),
            icon: Icon(Icons.delete, color: !isNavigatingActive ? Colors.black : Colors.grey),
            onPressed: isNavigatingActive ? null : onClearDrawing,
          ),
          for (final color in colors)
            _ColorSelectionIconButton(
              key: ValueKey<Color>(color),
              color: !isNavigatingActive ? color : Colors.grey,
              onPressed: isNavigatingActive
                  ? null
                  : (col) {
                      onColorChanged(col);
                    },
              isActive: activeColor == color,
            ),
        ],
      ),
    );
  }
}

class _ColorSelectionIconButton extends StatelessWidget {
  const _ColorSelectionIconButton({
    Key? key,
    required this.color,
    required this.onPressed,
    this.isActive = false,
  }) : super(key: key);

  final Color color;
  final ValueChanged<Color>? onPressed;
  final bool isActive;

  @override
  Widget build(BuildContext context) {
    return FeedbackIconButton(
      icon: Icon(isActive ? Icons.lens : Icons.panorama_fish_eye),
      color: color,
      onPressed: () => onPressed!(color),
    );
  }
}

class _ColumnDivider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 35,
      height: 1,
      color: Theme.of(context).dividerColor,
    );
  }
}
