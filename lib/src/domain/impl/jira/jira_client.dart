import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:retrofit/retrofit.dart';

part 'jira_client.g.dart';

Dio getJiraApiDio(String username, String apiToken) {
  final Dio dio = Dio();
  dio.interceptors.add(PrettyDioLogger(logPrint: (message) {
    print(message.toString());
  }));
  dio.options.headers.remove("User-Agent");
  dio.options.headers["X-Atlassian-Token"] = "nocheck";
  dio.options.headers["Content-Type"] = "application/json";
  dio.options.followRedirects = false;
  dio.options.validateStatus = (status) {
    return status != null && status < 500;
  };
  dio.interceptors.add(InterceptorsWrapper(
      onRequest: (RequestOptions options, RequestInterceptorHandler? handler) =>
          requestInterceptor(options, username, apiToken, handler)));
  return dio;
}

void requestInterceptor(RequestOptions options, String username,
    String apiToken, RequestInterceptorHandler? handler) async {
  final String basicValue = base64.encode(utf8.encode("$username:$apiToken"));
  options.headers.addAll({"Authorization": "Basic $basicValue"});
  handler?.next(options);
}

@RestApi()
abstract class JiraClient {
  factory JiraClient(Dio dio, {String baseUrl}) = _JiraClient;

  @POST("/rest/api/2/issue")
  Future<CreateIssueResponse> createIssue(@Body() CreateIssue body);

  @POST("/rest/api/2/issue/{key}/attachments")
  Future<void> postAttachment(@Path() String key, @Part() File file);
}

@JsonSerializable()
class CreateIssue {
  late Fields fields;

  CreateIssue({required this.fields});

  factory CreateIssue.fromJson(Map<String, dynamic> json) => _$CreateIssueFromJson(json);
  Map<String, dynamic> toJson() => _$CreateIssueToJson(this);
}

@JsonSerializable()
class Fields {
  String summary;
  String description;
  Project project;
  IssueType issuetype;

  Fields(
      {required this.summary,
      required this.description,
      required this.project,
      required this.issuetype});

  factory Fields.fromJson(Map<String, dynamic> json) => _$FieldsFromJson(json);
  Map<String, dynamic> toJson() => _$FieldsToJson(this);
}

@JsonSerializable()
class Project {
  String key;

  Project({required this.key});

  factory Project.fromJson(Map<String, dynamic> json) => _$ProjectFromJson(json);
  Map<String, dynamic> toJson() => _$ProjectToJson(this);
}

@JsonSerializable()
class IssueType {
  String name;

  IssueType({required this.name});

  factory IssueType.fromJson(Map<String, dynamic> json) => _$IssueTypeFromJson(json);
  Map<String, dynamic> toJson() => _$IssueTypeToJson(this);
}

@JsonSerializable()
class CreateIssueResponse {
  String id;
  String key;
  String self;

  CreateIssueResponse({required this.id, required this.key, required this.self});

  factory CreateIssueResponse.fromJson(Map<String, dynamic> json) =>
      _$CreateIssueResponseFromJson(json);
  Map<String, dynamic> toJson() => _$CreateIssueResponseToJson(this);
}
